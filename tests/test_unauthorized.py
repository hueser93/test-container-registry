# SPDX-FileCopyrightText: 2021 Helmholtz Centre for Environmental Research (UFZ)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: Apache-2.0

"""Test access to GitLab without authorization."""

from urllib.parse import urljoin

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement


def test_project_filter_unauthorized(browser: WebDriver, config):
    """Test if the explore page is working fine for unauthorized users."""
    project_explore_url: str = urljoin(config.getini("gitlab_url"), "explore")

    filter_field_id: str = "project-filter-form-field"
    project_slug: str = config.getini("unauthorized_explore_project_slug")
    expected_project_name: str = config.getini(
        "unauthorized_expected_explore_project_name"
    )

    browser.get(project_explore_url)
    filter_field: WebElement = browser.find_element(By.ID, filter_field_id)
    filter_field.send_keys(project_slug)
    filter_field.send_keys(Keys.RETURN)

    assert (
        expected_project_name in browser.page_source
    ), "Project could not be found. It might not exist or have a different name."


def test_search_unauthorized(browser: WebDriver, config):
    """Test if the search functionality works for unauthorized users."""
    project_explore_url: str = urljoin(config.getini("gitlab_url"), "explore")

    search_name: str = config.getini("unauthorized_search_name")
    search_field_id: str = "search"
    expected_project_name: str = config.getini(
        "unauthorized_expected_search_project_name"
    )

    browser.get(project_explore_url)
    search_field: WebElement = browser.find_element(By.ID, search_field_id)
    search_field.send_keys(search_name)
    search_field.send_keys(Keys.RETURN)

    assert (
        expected_project_name in browser.page_source
    ), "Project could not be found. It might not exist or have a different name."
