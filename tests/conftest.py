# SPDX-FileCopyrightText: 2021 Helmholtz Centre for Environmental Research (UFZ)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: Apache-2.0

"""Pytest configuration."""

import os

import pytest
from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver


def pytest_addoption(parser):
    """Add additional configuration that can be retrieved via getini."""
    parser.addini(
        "gitlab_url",
        "The GitLab URL to run the test with.",
        default="https://scm.hzdr.de",
    )
    parser.addini(
        "registry_url",
        "The GitLab Container Registry URL.",
        default="registry-stage.hzdr.de",
    )
    parser.addini(
        "public_project_path",
        "The path of a public project in the GitLab instance.",
        default="tobiashuste/test-project",
    )
    parser.addini(
        "unauthorized_explore_project_slug",
        "The project slug to filter for on the explore page.",
    )
    parser.addini(
        "unauthorized_expected_explore_project_name",
        "The project name expected to be present on the explore page.",
    )
    parser.addini(
        "unauthorized_search_name",
        "The project name to search for.",
    )
    parser.addini(
        "unauthorized_expected_search_project_name",
        "The project name expected to be present on the search results page.",
    )


@pytest.fixture(scope="function")
def browser() -> WebDriver:
    """Create the selenium webdriver."""
    driver_url: str = os.environ.get("SELENIUM_DRIVER_URL", "http://localhost:4444")
    firefox_options = webdriver.FirefoxOptions()
    driver: WebDriver = webdriver.Remote(
        command_executor=driver_url,
        options=firefox_options,
    )
    driver.maximize_window()
    yield driver
    driver.quit()


@pytest.fixture(scope="session", autouse=True)
def config(request):
    """Return a pytest configuration object."""
    return request.config


@pytest.fixture()
def gitlab_url() -> str:
    """Read the GitLab URL from the environment variable GITLAB_URL."""
    return os.environ.get("GITLAB_URL", "https://scm.hzdr.de")
