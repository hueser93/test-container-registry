# SPDX-FileCopyrightText: 2021 Helmholtz Centre for Environmental Research (UFZ)
# SPDX-FileCopyrightText: 2021 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: Apache-2.0

"""Test interaction via Git."""

import os
from urllib.parse import urljoin

import pygit2


def test_clone_https_public(config, tmp_path):
    """Check if a public repository can be cloned via HTTPS."""
    repo_url: str = urljoin(
        config.getini("gitlab_url"), f"{ config.getini('public_project_path') }.git"
    )
    repo_path: str = os.path.join(tmp_path, "repository")

    pygit2.clone_repository(repo_url, repo_path)

    assert os.path.exists(
        os.path.join(repo_path, ".git")
    ), "Directory does not contain a folder .git. Cloning a repository failed."
