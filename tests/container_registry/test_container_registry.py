# SPDX-FileCopyrightText: 2022 Helmholtz Centre for Environmental Research (UFZ)
# SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)
#
# SPDX-License-Identifier: Apache-2.0

"""Test usage of GitLab Container Registry."""

import os

import docker
import pytest


@pytest.fixture(scope="session")
def client() -> docker.DockerClient:
    return docker.from_env()


@pytest.fixture(scope="session")
def tag(config) -> str:
    return config.getini("registry_url") + "/" + config.getini("public_project_path")


@pytest.mark.container_registry
def test_container_registry_login(client, config):
    """Test logging in into GitLab container registry."""
    login_status = client.login(
        username=os.environ.get("CIUSER"),
        password=os.environ.get("CIPASSWD"),
        registry=config.getini("registry_url"),
        reauth=True,
    )
    assert "Login Succeeded" in login_status["Status"], "Login failed: " + login_status


@pytest.mark.container_registry
def test_container_registry_pull_from_docker(client):
    """Test pulling from Docker Hub."""
    image = client.images.pull("busybox")
    assert "busybox:latest" in image.attrs["RepoTags"], (
        "Pull failed: " + image.attrs["RepoTags"]
    )


@pytest.mark.container_registry
def test_container_registry_build_locally(client, tag):
    """Test building image."""
    build_status = client.images.build(path=".", tag=tag)
    assert tag in build_status[0].attrs["RepoTags"][0], (
        "Built failed: " + build_status[0].attrs["RepoTags"][0]
    )


@pytest.mark.container_registry
def test_container_registry_push_to_gitlab(client, tag):
    """Test pushing image to GitLab Container Registry."""
    push_result = client.api.push(repository=tag, stream=False, decode=True)
    assert tag in push_result, "Push failed: " + push_result


@pytest.mark.container_registry
def test_container_registry_pull_from_gitlab(client, tag):
    """Test pulling from GitLab Container Registry."""
    image = client.images.pull(repository=tag, tag="latest")
    assert tag + ":latest" in image.attrs["RepoTags"], (
        "Pull failed: " + image.attrs["RepoTags"][0]
    )


@pytest.mark.container_registry
def test_container_registry_remove_locally(client, tag):
    """Test removing local image."""
    remove_result = client.images.remove(image=tag)
    assert remove_result is None, "Remove failed: " + remove_result
